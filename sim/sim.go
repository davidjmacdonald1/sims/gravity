package sim

import (
	"image/color"
	"math"

	"github.com/deeean/go-vector/vector2"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Vector2 = vector2.Vector2

const G = 6.67430e-11 // Nm^2/kg^2

const UpdatesPerSec = 60   // 1/s
const Width = 1080         // px
const Height = 720         // px
const OriginX = Width / 2  // px
const OriginY = Height / 2 // px
var Scale = 3e-2           // m/px

func PositionToPixels(p *Vector2) (x, y float32) {
	x = float32(p.X/Scale + OriginX)
	y = float32(Height - (p.Y/Scale + OriginY))
	return x, y
}

func PositionToMeters(x, y float64) *Vector2 {
	xm := (x - OriginX) * Scale
	ym := (Height - y - OriginY) * Scale
	return vector2.New(xm, ym)
}

func DrawVector(dst *ebiten.Image, p, v *Vector2, clr color.Color) {
	v = v.Normalize().MulScalar(math.Log2(v.Magnitude()))
	end := p.Add(v)

	w := float32(v.Magnitude() / Scale / 15)
	DrawLine(dst, p, end, w, clr)

	mid := p.Add(v.MulScalar(.9))
	n := vector2.New(-v.Y, v.X).Normalize().MulScalar(v.Magnitude() / 10)

	DrawLine(dst, end, mid.Add(n), w, clr)
	DrawLine(dst, end, mid.Sub(n), w, clr)
}

func DrawLine(dst *ebiten.Image, p1, p2 *Vector2, w float32, clr color.Color) {
	x1, y1 := PositionToPixels(p1)
	x2, y2 := PositionToPixels(p2)
	vector.StrokeLine(dst, x1, y1, x2, y2, w, clr, true)
}
