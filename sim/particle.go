package sim

import (
	"image/color"

	"github.com/deeean/go-vector/vector2"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Particle struct {
	M   float64  // kg
	R   float64  // m
	Pos *Vector2 // m
	Vel *Vector2 // m/s
}

func MakeParticle(m, r, x, y, vx, vy float64) Particle {
	return Particle{
		M:   m,
		R:   r,
		Pos: vector2.New(x, y),
		Vel: vector2.New(vx, vy),
	}
}

func (p *Particle) Update() {
	p.Move()
}

func (p Particle) Draw(dst *ebiten.Image, isSelected bool) {
	r := float32(p.R / Scale)
	x, y := PositionToPixels(p.Pos)
	vector.DrawFilledCircle(dst, x, y, r, color.White, true)

	if isSelected {
		red := color.RGBA{R: 255, A: 255}
		vector.DrawFilledCircle(dst, x, y, r/10, red, true)
		DrawVector(dst, p.Pos, p.Vel, red)
	}
}

func (p *Particle) Move() {
	p.Pos = p.Pos.Add(p.Vel.DivScalar(UpdatesPerSec))
}

func (p *Particle) Impulse(v *Vector2) {
	p.Vel = p.Vel.Add(v)
}

func (p *Particle) Accelerate(a *Vector2) {
	p.Impulse(a.DivScalar(UpdatesPerSec))
}
