package sim

import (
	"gonum.org/v1/gonum/stat/combin"
)

type Interaction struct {
	P1, P2             *Particle
	v1Scalar, v2Scalar float64
	minDistance        float64
}

func MakeInteraction(p1, p2 *Particle) Interaction {
	mSum := p1.M + p2.M

	return Interaction{
		P1:          p1,
		P2:          p2,
		v1Scalar:    (p1.M - p2.M) / mSum,
		v2Scalar:    2 * p1.M / mSum,
		minDistance: p1.R + p2.R,
	}
}

func GenerateInteractions(particles []Particle) []Interaction {
	if len(particles) < 2 {
		return []Interaction{}
	}

	interactions := make([]Interaction, 0, combin.Binomial(len(particles), 2))

	for i := range particles {
		for j := i + 1; j < len(particles); j++ {
			interaction := MakeInteraction(&particles[i], &particles[j])
			interactions = append(interactions, interaction)
		}
	}

	return interactions
}

func (i *Interaction) Update() {
	r := i.P1.Pos.Sub(i.P2.Pos)
	rMag := r.Magnitude()

	if !i.AttemptCollision(r, rMag) {
		i.Attract(r, rMag)
	}
}

func (i *Interaction) AttemptCollision(r *Vector2, rMag float64) bool {
	v := i.P1.Vel.Sub(i.P2.Vel)

	if rMag > i.minDistance || v.Dot(r) > 0 {
		return false
	}

	v1, v2 := v.MulScalar(i.v1Scalar), v.MulScalar(i.v2Scalar)
	i.P1.Impulse(v1.Sub(v))
	i.P2.Impulse(v2)

	return true
}

func (i *Interaction) Attract(r *Vector2, rMag float64) {
	g := r.DivScalar(rMag * rMag * rMag / G)
	i.P1.Accelerate(g.MulScalar(-i.P2.M))
	i.P2.Accelerate(g.MulScalar(i.P1.M))
}
