package main

import (
	"fmt"
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"gitlab.com/davidjmacdonald1/sims/gravity/sim"
)

func DrawAxes(screen *ebiten.Image) {
	ox, oy := float32(sim.OriginX), float32(sim.OriginY)
	grey := color.RGBA{R: 55, G: 55, B: 55, A: 20}
	vector.StrokeLine(screen, 0, oy, sim.Width, oy, 1, grey, false)
	vector.StrokeLine(screen, ox, 0, ox, sim.Height, 1, grey, false)

	width := float32(4)
	scale := math.Log10(sim.Scale)
	shiftMeters := math.Pow10(int(scale + 2))
	shift := float32(shiftMeters / sim.Scale)

	str := fmt.Sprintf("%1.0em", shiftMeters)

	xi := ox - float32(math.Floor(float64(ox/shift)))*shift
	yi := oy - float32(math.Floor(float64(oy/shift)))*shift

	ebitenutil.DebugPrintAt(screen, str, int(ox+shift), int(oy+5))

	for x := xi; x < sim.Width; x += shift {
		if int(x) == int(ox) {
			continue
		}

		vector.StrokeLine(screen, x, oy+width-1, x, oy-width, 1, grey, false)
	}

	for y := yi; y < sim.Height; y += shift {
		if int(y) == int(oy) {
			continue
		}

		vector.StrokeLine(screen, ox+width-1, y, ox-width, y, 1, grey, false)
	}
}
