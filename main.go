package main

import (
	"fmt"
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/davidjmacdonald1/sims/gravity/sim"
)

type Game struct {
	particles    []sim.Particle
	interactions []sim.Interaction
	selected     *sim.Particle
}

func (g *Game) Update() error {
	_, dy := ebiten.Wheel()
	sim.Scale -= sim.Scale * dy / 10

	for i := range g.interactions {
		g.interactions[i].Update()
	}

	for i := range g.particles {
		g.particles[i].Update()
	}

	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	DrawAxes(screen)

	cx, cy := ebiten.CursorPosition()
	c := sim.PositionToMeters(float64(cx), float64(cy))

	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonRight) {
		g.selected = nil
	}

	for i := range g.particles {
		p := &g.particles[i]
		isSelected := g.selected == p
		p.Draw(screen, isSelected)

		isHovered := p.Pos.Sub(c).Magnitude() < p.R

		if isHovered && ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
			g.selected = p
		}

		if g.selected == nil && isHovered || isSelected {
			s := "<%1.3f, %1.3f> or %1.3fm/s"
			v := fmt.Sprintf(s, p.Vel.X, p.Vel.Y, p.Vel.Magnitude())
			ebitenutil.DebugPrintAt(screen, v, sim.OriginX-100, 10)
		}
	}

	scale := fmt.Sprintf("%1.1em/px", sim.Scale)
	ebitenutil.DebugPrintAt(screen, scale, 10, 10)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return outsideWidth, outsideHeight
}

func main() {
	ebiten.SetWindowSize(sim.Width, sim.Height)
	ebiten.SetWindowTitle("Gravity Simulation")

	x := 10.0

	particles := []sim.Particle{
		sim.MakeParticle(20e12, 2, x, x, 0, 0),
		sim.MakeParticle(20e12, 2, -x, x, 0, 0),
		sim.MakeParticle(20e12, 2, x, -x, 0, 0),
		sim.MakeParticle(20e12, 2, -x, -x, 0, 0),
	}

	game := &Game{
		particles:    particles,
		interactions: sim.GenerateInteractions(particles),
	}

	err := ebiten.RunGame(game)
	if err != nil {
		log.Fatal(err)
	}
}
